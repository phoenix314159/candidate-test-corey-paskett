import React from 'react'
import ReactDOM from 'react-dom'
import App from './components/App'
import axios from 'axios'

const getData = async () => {
  const {data} = await axios({
    method: 'GET',
    headers: {
      'Content-Type': 'application/json'
    },
    url:'/api/get_data'
  })
  return data
}

ReactDOM.render(<App data={getData}/>, document.getElementById('app'))

