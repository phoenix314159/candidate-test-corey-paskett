import React from 'react'

const Thead = () => (
  <thead>
  <tr className="head">
    <td>Year</td>
    <td>Actress</td>
    <td>Movie</td>
  </tr>
  </thead>
)

export default Thead
