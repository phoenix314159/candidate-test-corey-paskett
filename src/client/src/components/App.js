import React, { Component } from 'react'
import '../../node_modules/bootstrap/dist/css/bootstrap.min.css'
import '../styles/css/App.css'
import PopUp from './PopUp'
import Thead from './Thead'
import Paginate from 'react-js-pagination'

export default class App extends Component {
  constructor (props) {
    super(props)
    this.state = {
      data: [],
      show: false,
      index: 0,
      activePage: 1
    }
  }

  async componentDidMount () {
    const data = await this.props.data()
    this.setState({data: data.slice(0, 10)}) // this.state.data will be an array of the first 10
  }                                         // objects when promise is resolved

  openPopUp (index) { // show data depending on row clicked
    this.setState({show: true, index})
  }

  closePopUp () {
    this.setState({show: false})
  }

  paginate (activePage, data) {
    this.setState({activePage, data})
  }

  handlePageChange = async (pageNumber) => {
    const data = await this.props.data()
    switch (pageNumber) {
      case 5:
        this.paginate(pageNumber, data.slice(40))  //on last page only display last 7 items
        break
      default:  // show 10 items per page depending on page clicked
        this.paginate(pageNumber, data.slice((pageNumber - 1) * 10, pageNumber * 10))
        break
    }
  }

  render () {
    const {data, show, index, activePage} = this.state
    if (!data) return <div>Loading...</div> // wait for data to come back then render component
    if (show) {
      const {year, name, movie} = data[index]  //pull off properties of any index that is clicked
      return (
        <PopUp year={year} actress={name} movie={movie} close={() => this.closePopUp()}/>
      )
    }

    return (
      <div className="total">
        <h1>Academy Award Winning Actresses</h1>
        <div className="moveTable">
          <table className="actressTable">
            <Thead />
            <tbody>
            {data.map((row, i) => { //iterate over json object from server
              const {year, name, movie} = row  //pull off properties to display on every iteration
              return (
                <tr key={i} onClick={() => this.openPopUp(i)}>
                  <td>{year}</td>
                  <td>{name}</td>
                  <td>{movie}</td>
                </tr>
              )
            })}
            </tbody>
          </table>
        </div>
        <div className="movePag">
          <Paginate activePage={activePage}
                    itemsCountPerPage={10}
                    totalItemsCount={47}
                    pageRangeDisplayed={5}
                    onChange={this.handlePageChange}/>
        </div>
      </div>
    )
  }
}


