import React from 'react'

const PopUp = ({actress, movie, year, close}) => (
  <div className='popup'>
    <div className='popup_inner'>
      <p>{`${actress} acted in "${movie}" in the year ${year}.`}</p>
      <button className="btn btn-primary" onClick={close}>OK</button>
    </div>
  </div>
)

export default PopUp