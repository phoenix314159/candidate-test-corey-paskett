package server;

public class Actress {

    private String year;
    private String name;
    private String movie;

    public Actress(String year, String name, String movie) {
        this.year = year;
        this.name = name;
        this.movie = movie;
    }
}
