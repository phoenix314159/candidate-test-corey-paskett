package server;

import java.io.*;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.google.gson.Gson;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/api/get_data")

public class ServerController {
    @RequestMapping(method = RequestMethod.GET)
    public @ResponseBody
    String getData() {
        String jsonData = "";
        List<Actress> actresses;
        Pattern pattern = Pattern.compile(",");
        String csvFile = "src/main/resources/academy_award_actresses.csv";

        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(csvFile))) {
            actresses = bufferedReader.lines().skip(1).map(lineOfText -> {
                String[] row = pattern.split(lineOfText);
                return new Actress(row[0].replace("\"", ""), row[1].replace("\"", ""), row[2].replace("\"", ""));
            }).collect(Collectors.toList());
            Gson gson = new Gson();
            jsonData = gson.toJson(actresses);

        } catch (JsonGenerationException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return jsonData;
    }
}